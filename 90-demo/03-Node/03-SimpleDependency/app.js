var _ = require('lodash');
var calculator = require('./lib/calculator');


console.log('Starting calculation ...');

var input = [1,2,3,4];

var output = calculator.doComplicatedCalculation(input);

console.log('Output: ' + output);

var result = _.uniq(output);

console.log('Result: ' + result);

console.log('Finished calculation.');