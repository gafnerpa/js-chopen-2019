var express = require('express');
var lib = require('./lib');

var app = express();

app.get('/', function(req, res) {
    res.send(lib.createMessage());
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});