function doComplicatedCalculation(input){

    var output = [];

    for (var i = 0; i < input.length; i++){
        var val = input[i];
        output.push(i*i, i*i*i);
    }

    return output;

}

exports.doComplicatedCalculation = doComplicatedCalculation;